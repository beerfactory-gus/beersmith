# beersmith

Beersmith Repository

## How to Build

```docker
docker build -t gucarreira:beersmith:3.0.9 .
```

## How to run

As root do:

```bash
export DISPLAY=:0.0
xhost +
```

```docker
docker run -ti \
  --init -e DISPLAY=$DISPLAY \
  -u $UID:`id -g $USER` \
  -v $HOME/.beersmith2:/home/beersmith3/.beersmith2 \
  -v $HOME/.beersmith3:/home/beersmith3/.beersmith3 \
  -v $HOME/Documents:/home/beersmith3/Documents \
  -v /var/run/cups/cups.sock:/var/run/cups/cups.sock \
  -v /tmp/.X11-unix:/tmp/.X11-unix \
  -v /dev/snd:/dev/snd:rw \
  --name beersmith3 \
  gucarreira/beersmith:3.0.9
```
